﻿#if true
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManagerTest : MonoBehaviour
{
    public Slider leftJoystickX, leftJoystickY;
    public Slider rightJoystickX, rightJoystickY;

    public Transform aButton, bButton, xButton, yButton;

    private void Update()
    {
        // Left Joystick
        leftJoystickX.value = InputManager.LeftHorizontal();
        leftJoystickY.value = InputManager.LeftVertical();

        // Right Joystick
        rightJoystickX.value = InputManager.RightHorizontal();
        rightJoystickY.value = InputManager.RightVertical();

        // A Button
        aButton.GetChild(0).gameObject.SetActive(InputManager.IsDown(InputName.A));
        aButton.GetChild(1).gameObject.SetActive(InputManager.IsHeld(InputName.A));
        aButton.GetChild(2).gameObject.SetActive(InputManager.IsUp(InputName.A));

        // B Button
        bButton.GetChild(0).gameObject.SetActive(InputManager.IsDown(InputName.B));
        bButton.GetChild(1).gameObject.SetActive(InputManager.IsHeld(InputName.B));
        bButton.GetChild(2).gameObject.SetActive(InputManager.IsUp(InputName.B));

        // X Button
        xButton.GetChild(0).gameObject.SetActive(InputManager.IsDown(InputName.X));
        xButton.GetChild(1).gameObject.SetActive(InputManager.IsHeld(InputName.X));
        xButton.GetChild(2).gameObject.SetActive(InputManager.IsUp(InputName.X));

        // Y Button
        yButton.GetChild(0).gameObject.SetActive(InputManager.IsDown(InputName.Y));
        yButton.GetChild(1).gameObject.SetActive(InputManager.IsHeld(InputName.Y));
        yButton.GetChild(2).gameObject.SetActive(InputManager.IsUp(InputName.Y));
    }
}
#endif